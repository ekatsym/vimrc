" general
set nocompatible
set number
set list
set listchars=eol:$,trail:-
set hlsearch
set expandtab tabstop=2 shiftwidth=2 softtabstop=2
set autoindent
set wildmode=longest:list,full
filetype plugin indent on
syntax on
set showmatch matchtime=1
let loaded_matchparen=1
let mapleader = ","
let maplocalleader = ","
let &t_TI = ""
let &t_TE = ""

" Plugin
call plug#begin('~/.vim/plugged')
" General
Plug 'Shougo/vimproc.vim', {'do' : 'make'}                          " Asynchronous execution library
Plug 'Shougo/unite.vim'                                             " Search and display information from files, buffers, and so on.
Plug 'Shougo/vimshell.vim'                                          " Shell on Vim
Plug 'itchyny/vim-parenmatch'                                       " Match parentheses
Plug 'Yggdroot/indentLine'                                          " Show indent
Plug 'scrooloose/nerdtree'                                          " Left side buffer filer
Plug 'jistr/vim-nerdtree-tabs'                                      " NerdTree enhancer
Plug 'itchyny/lightline.vim'                                        " Empowerment status bar
Plug 'ctrlpvim/ctrlp.vim'                                           " Ctrl-P Filer
Plug 'kovisoft/paredit', {'for': ['scheme', 'racket', 'clojure']}   " Paredit
Plug 'luochen1990/rainbow'                                          " Rainbow Parentheses

" Language Server Protocol
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp',              {'for': ['haskell', 'fsharp', 'cs', 'typescript', 'c', 'cpp', 'python', 'go']}
Plug 'prabirshrestha/asyncomplete.vim',     {'for': ['haskell', 'fsharp', 'cs', 'typescript', 'c', 'cpp', 'python', 'go']}
Plug 'prabirshrestha/asyncomplete-lsp.vim', {'for': ['haskell', 'fsharp', 'cs', 'typescript', 'c', 'cpp', 'python', 'go']}
Plug 'mattn/vim-lsp-settings',              {'for': ['haskell', 'fsharp', 'cs', 'typescript', 'c', 'cpp', 'python', 'go']}

" Programming languages
Plug 'kovisoft/slimv',              {'for': 'lisp'}     " Common Lisp
Plug 'wlangstroth/vim-racket',      {'for': 'racket'}   " Racket
Plug 'liquidz/vim-iced',            {'for': 'clojure'}  " Clojure
Plug 'neovimhaskell/haskell-vim',   {'for': 'haskell'}  " Haskell
Plug 'rust-lang/rust.vim',          {'for': 'rust'}     " Rust
Plug 'ionide/Ionide-vim',           {'for': 'fsharp'}   " F#
Plug 'LumaKernel/coquille',         {'for': 'coq'}      " Coq
Plug 'hylang/vim-hy',               {'for': 'hy'}       " Hy

" Markup languages
Plug 'vim-pandoc/vim-pandoc',           {'for': 'pandoc'}   " Markdown
Plug 'vim-pandoc/vim-pandoc-syntax',    {'for': 'pandoc'}   " Markdown
Plug 'lervag/vimtex',                   {'for': 'tex'}      " TeX
Plug 'qnighy/satysfi.vim',              {'for': 'satysfi'}  " SATySFi

" Other languages
Plug 'othree/xml.vim',                  {'for': 'xml'}      " XML
Plug 'othree/html5.vim',                {'for': 'html'}     " HTML5
Plug 'hail2u/vim-css3-syntax',          {'for': 'css'}      " CSS3
Plug '~/Sources/llvm-project-llvmorg/llvm/utils/vim'        " LLVM
Plug 'vim-scripts/gnuplot.vim'                              " Gnuplot
Plug 'vim-scripts/gtk-vim-syntax'                           " Gtk
Plug 'liuchengxu/graphviz.vim'                              " Graphviz

" colorscheme
Plug 'zefei/simple-dark'
"Plug 'haishanh/night-owl.vim

call plug#end()

" alias
nnoremap <silent><C-t> :tabnew<Return>
nnoremap <silent><C-b>b :%!xxd<Return>:set ft=xxd<Return>
nnoremap <silent><C-b>r :%!xxd -r<Return>
cnoremap $reload<Return> :w<Return>:source ~/.vimrc<Return>:e<Return>
"" vimshell
nnoremap <silent><C-s> :VimShell<Return>
"" nerdtree
nnoremap <silent><C-n> :NERDTreeToggle<Return>
cnoremap $path<Return> :echo expand('%:p')<Return>


" simple functions
function! Simple_TabComplete()
    let line = getline('.')
    let substr = strpart(line, -1, col('.')+1)
    let substr = matchstr(substr, "[^ \t]*$")
    if (strlen(substr)==0)
        return "\<TAB>"
    endif
    return "\<C-X>\<C-O>"
endfunction

" colorscheme
colorscheme simple-dark
highlight Pmenu ctermfg=250 ctermbg=237
highlight Normal ctermbg=none
highlight NonText ctermbg=none
highlight LineNr ctermbg=none
highlight Folded ctermbg=none
highlight EndOfBuffer ctermbg=none

" lightline
set laststatus=2
set ambiwidth=double
set noshowmode
let g:lightline = { 'colorscheme': 'wombat' }

" indentLine
let g:indentLine_char = '| '
let g:indentLine_enabled = 0

" Language Server Protocol
autocmd FileType haskell,fsharp,typescript,c,cpp,cs,python,go nnoremap <silent><LocalLeader>s :LspHover<Return>
autocmd FileType haskell,fsharp,typescript,c,cpp,cs,python,go nnoremap <silent><LocalLeader>t :LspPeekDefinition<Return>
autocmd FileType haskell,fsharp,typescript,c,cpp,cs,python,go nnoremap <silent><LocalLeader>F :LspDocumentDiagnostics<Return>
autocmd FileType haskell,fsharp,typescript,c,cpp,cs,python,go nnoremap <silent><C-]> :LspDefinition<Return>

" asyncomplete
let g:asyncomplete_auto_popup = 0

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction

autocmd FileType haskell,fsharp,c,cpp,cs,python,go inoremap <silent><expr> <TAB>
  \ pumvisible() ? "\<C-n>" :
  \ <SID>check_back_space() ? "\<TAB>" :
  \ asyncomplete#force_refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

" rainbow
let g:rainbow_active = 0
let g:rainbow_conf = {
\   'separately': {
\       'scheme': {
\           'guifgs': ['red', 'yellow', 'green', 'cyan', 'magenta'],
\           'ctermfgs': ['red', 'yellow', 'green', 'cyan', 'magenta'],
\       },
\       'racket': {
\           'guifgs': ['red', 'yellow', 'green', 'cyan', 'magenta'],
\           'ctermfgs': ['red', 'yellow', 'green', 'cyan', 'magenta'],
\       },
\   }
\}

" Common Lisp
let g:lisp_rainbow = 1
autocmd BufNewFile,BufRead *.asd set filetype=lisp
autocmd BufNewFile,BufRead *.ros set filetype=lisp
autocmd FileType lisp setl tabstop=2 shiftwidth=2 softtabstop=2
"" Slimv
autocmd FileType lisp let g:slimv_builtin_swank = 1
autocmd FileType lisp let g:slimv_swank_cmd = "!ros -e '(ql:quickload :swank) (swank:create-server)' wait &"
autocmd FileType lisp let g:slimv_lisp = 'ros run'
autocmd FileType lisp let g:slimv_impl = 'sbcl'
autocmd FileType lisp let g:slimv_repl_split = 4
autocmd FileType lisp let g:slimv_repl_split_size = 80
autocmd FileType lisp let g:slimv_repl_simple_eval = 0

" Scheme
autocmd FileType scheme setl tabstop=2 shiftwidth=2 softtabstop=2
autocmd FileType scheme nnoremap <silent><LocalLeader>c :VimShellInteractive gosh -r7<Return><Esc><C-w>L:vertical resize 60<Return><C-w>h
autocmd FileType scheme nnoremap <silent><LocalLeader>e :VimShellSendString<Return>
autocmd FileType scheme vnoremap <silent><LocalLeader>e :VimShellSendString<Return>
autocmd FileType scheme nnoremap <silent><LocalLeader>b :VimShellSendBuffer<Return>
autocmd FileType scheme nnoremap <silent><LocalLeader>Q :VimShellClose<Return>

" Racket
autocmd FileType racket nnoremap <silent><LocalLeader>c :VimShellInteractive racket<Return><Esc><C-w>L:vertical resize 60<Return><C-w>h
autocmd FileType racket nnoremap <silent><LocalLeader>e :VimShellSendString<Return>
autocmd FileType racket vnoremap <silent><LocalLeader>e :VimShellSendString<Return>
autocmd FileType racket nnoremap <silent><LocalLeader>b :VimShellSendBuffer<Return>
autocmd FileType racket nnoremap <silent><LocalLeader>Q :VimShellClose<Return>

" ACL2
autocmd BufNewFile,BufRead *.acl2 let g:slimv_keybindings=0
autocmd BufNewFile,BufRead *.acl2 set filetype=lisp
autocmd BufNewFile,BufRead *.acl2 nnoremap <silent><LocalLeader>c :VimShellInteractive acl2<Return><Esc><C-w>L:vertical resize 60<Return><C-w>h
autocmd BufNewFile,BufRead *.acl2 nnoremap <silent><LocalLeader>e :VimShellSendString<Return>
autocmd BufNewFile,BufRead *.acl2 vnoremap <silent><LocalLeader>e :VimShellSendString<Return>
autocmd BufNewFile,BufRead *.acl2 nnoremap <silent><LocalLeader>d :VimShellSendString<Return>
autocmd BufNewFile,BufRead *.acl2 vnoremap <silent><LocalLeader>d :VimShellSendString<Return>
autocmd BufNewFile,BufRead *.acl2 nnoremap <silent><LocalLeader>b :VimShellSendBuffer<Return>
autocmd BufNewFile,BufRead *.acl2 nnoremap <silent><LocalLeader>Q :VimShellClose<Return>

" Clojure
" vim-iced
let g:iced_enable_default_key_mappings = v:true

" Haskell
" haskell-language-server
if executable('haskell-language-server-wrapper')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'haskell-language-server-wrapper',
        \ 'cmd': {server_info->['haskell-language-server-wrapper', '--lsp']},
        \ 'root_uri':{server_info->lsp#utils#path_to_uri(
        \     lsp#utils#find_nearest_parent_file_directory(
        \         lsp#utils#get_buffer_path(),
        \         ['.cabal', 'stack.yaml', 'cabal.project', 'package.yaml', 'hie.yaml', '.git'],
        \     ))},
        \ 'whitelist': ['haskell', 'lhaskell'],
        \ })
endif

" OCaml
autocmd FileType ocaml setl tabstop=2 shiftwidth=2 softtabstop=2
autocmd FileType ocaml nnoremap <silent><LocalLeader>s :MerlinDocument<Return>
autocmd FileType ocaml nnoremap <silent><LocalLeader>F :MerlinErrorCheck<Return>
autocmd FileType ocaml nnoremap <silent><C-]> :MerlinLocate<Return>
autocmd FileType ocaml inoremap <expr><TAB> Simple_TabComplete()

" Python
autocmd FileType python nnoremap <silent><LocalLeader>c :VimShellInteractive python3<Return><Esc><C-w>L:vertical resize 60<Return><C-w>h
autocmd FileType python nnoremap <silent><LocalLeader>e :VimShellSendString<Return>
autocmd FileType python vnoremap <silent><LocalLeader>e :VimShellSendString<Return>
autocmd FileType python nnoremap <silent><LocalLeader>b :VimShellSendBuffer<Return>
autocmd FileType python nnoremap <silent><LocalLeader>Q :VimShellClose<Return>

" Hy
autocmd FileType hy nnoremap <silent><LocalLeader>c :VimShellInteractive hy<Return><Esc><C-w>L:vertical resize 60<Return><C-w>h
autocmd FileType hy nnoremap <silent><LocalLeader>e :VimShellSendString<Return>
autocmd FileType hy vnoremap <silent><LocalLeader>e :VimShellSendString<Return>
autocmd FileType hy nnoremap <silent><LocalLeader>b :VimShellSendBuffer<Return>
autocmd FileType hy nnoremap <silent><LocalLeader>Q :VimShellClose<Return>

" C C++
autocmd FileType c inoremap { {<Return>}<Esc>O
autocmd FileType cpp inoremap { {<Return>}<Esc>O

" C#
autocmd FileType cs setl tabstop=4 shiftwidth=4 softtabstop=4

" CUDA
autocmd FileType cuda inoremap { {<Return>}<Esc>O

" Markdown
" vim-pandoc
let g:pandoc#syntax#protect#codeblocks = 0
let g:pandoc#modules#disabled = ["folding"]
let g:pandoc#spell#enabled = 0
let g:pandoc#syntax#conceal#use = 0
let g:pandoc#syntax#codeblocks#embeds#langs = [
            \ "lisp",
            \ "scheme",
            \ "clojure",
            \ "haskell",
            \ "ocaml",
            \ "c",
            \ "cpp",
            \ "go",
            \ "rust",
            \ "python",
            \ ]
let g:markdown_fenced_languages = ["csharp=cs"]

" HTML
autocmd FileType html setl expandtab tabstop=2 shiftwidth=2 softtabstop=2

" YAML
autocmd FileType yaml setl expandtab tabstop=2 shiftwidth=2 softtabstop=2

" SATySFi
autocmd FileType satysfi setl expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd FileType satysfi inoremap <C-y>doc StdJa.document(\|<Return>title<Space>=<Space>{};<Return>author<Space>=<Space>{};<Return>show-title<Space>=<Space>true;<Return>show-toc<Space>=<Space>true;<Return>\|)'<<Return>><Esc>O
autocmd FileType satysfi inoremap <C-y>p +p{<Return>}<Esc>O
autocmd FileType satysfi inoremap <C-y>np +pn{<Return>}<Esc>O
autocmd FileType satysfi inoremap <C-y>tp +ptitle{}{<Return>}<Esc>k$hi
autocmd FileType satysfi inoremap <C-y>sec +section{}<<Return>><Esc>k$hi
autocmd FileType satysfi inoremap <C-y>cha +chapter{}<<Return>><Esc>k$hi
autocmd FileType satysfi inoremap <C-y>ssec +subsection{}<<Return>><Esc>k$hi
autocmd FileType satysfi inoremap <C-y>sssec +subsubsection{}<<Return>><Esc>k$hi
autocmd FileType satysfi inoremap <C-y>math +math(${<Return>});<Esc><Tab>  
autocmd FileType satysfi inoremap <C-y>fig +figure?:(`fig:`)(7cm)(``){}<Esc>3hi
autocmd FileType satysfi inoremap <C-y>href \href(`url`){text}<Esc>7hi
autocmd FileType satysfi inoremap <C-y>ref \ref(`id`)<Esc>hi
autocmd FileType satysfi inoremap <C-y>+code +code(`<Return>`);<Esc>O<Tab>
autocmd FileType satysfi inoremap <C-y>\code \code(``);<Esc>2hi
autocmd FileType satysfi inoremap <C-y>dl +dialogue{}{<Return>}<Esc>k$hi
" TeX
let g:tex_flavor = 'uplatex'
let g:vimtex_compiler_latexmk_engines = { '_' : '-pdfdvi' }
autocmd FileType tex setl expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd FileType tex inoremap <C-y>b \begin{}<Esc>i
